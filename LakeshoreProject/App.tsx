// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * Generated with the TypeScript template
//  * https://github.com/react-native-community/react-native-template-typescript
//  *
//  * @format
//  */

// import React from 'react';
// import {
//   Image,
//   SafeAreaView,
//   ScrollView,
//   StatusBar,
//   StyleSheet,
//   Text,
//   useColorScheme,
//   View,
// } from 'react-native';

// import {
//   Colors,
//   DebugInstructions,
//   Header,
//   LearnMoreLinks,
//   ReloadInstructions,
// } from 'react-native/Libraries/NewAppScreen';

// const Section: React.FC<{
//   title: string;
// }> = ({children, title}) => {
//   const isDarkMode = useColorScheme() === 'dark';
//   return (
//     <View style={styles.sectionContainer}>
//       <Text
//         style={[
//           styles.sectionTitle,
//           {
//             color: isDarkMode ? Colors.white : Colors.black,
//           },
//         ]}>
//         {title}
//       </Text>
//       <Text
//         style={[
//           styles.sectionDescription,
//           {
//             color: isDarkMode ? Colors.light : Colors.dark,
//           },
//         ]}>
//         {children}
//       </Text>
//     </View>
//   );
// };

// const App = () => {
//   const isDarkMode = useColorScheme() === 'dark';

//   const backgroundStyle = {
//     backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
//   };

//   return (
    // <SafeAreaView style={backgroundStyle}>
    //   {/* <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} /> */}
    //   <ScrollView
    //     contentInsetAdjustmentBehavior="automatic"
    //     style={backgroundStyle}>
    //     <View
    //       style={{ padding: 10, flex: 1,
    //         flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: 'grey'
    //       }}>
    //       <Image source={Glyphs.fbLogo} style={{height: 15, width: 15}}/>
    //       <View style={{alignItems: 'center'}}>
    //       <Text style={{fontWeight: 'bold'}}>App bar title</Text>
    //       <Text>App bar subTitle</Text>
    //       </View>
    //       <Text style={{color: '#4da2c3'}}>Edit</Text>
    //     </View>
    //       <View style={{margin: 10, backgroundColor: 'red', height: 45, alignItems: 'center', justifyContent: 'center'}}>
    //         <Text style={{color: 'white', fontWeight: 'bold'}}>Join Now</Text>
    //       </View>
    //       <View style={{margin: 10, backgroundColor: 'white', height: 45, alignItems: 'center', borderColor: '#1b4878', borderWidth: 1, justifyContent: 'center'}}>
    //         <Text style={{color: '#1b4878', fontWeight: 'bold'}}>Sign In</Text>
    //       </View>
    //       <Text style={{color: '#1b4878', fontWeight: 'bold'}}>Not Now</Text>

    //   </ScrollView>
    // </SafeAreaView>
//   );
// };

// const styles = StyleSheet.create({
//   sectionContainer: {
//     marginTop: 32,
//     paddingHorizontal: 24,
//   },
//   sectionTitle: {
//     fontSize: 24,
//     fontWeight: '600',
//   },
//   sectionDescription: {
//     marginTop: 8,
//     fontSize: 18,
//     fontWeight: '400',
//   },
//   highlight: {
//     fontWeight: '700',
//   },
// });

// export default App;


// export const Glyphs = {
//   fbLogo: require('../LakeshoreProject/app/assets/images/apple.png')
// }

import React from 'react';
import Routes from './app/routes/routes';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {store, persistor} from './app/redux/store';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Routes />
      </PersistGate>
    </Provider>
  );
};

export default App;
