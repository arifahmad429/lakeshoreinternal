import {CommonActions} from '@react-navigation/native';
import {store} from '../redux/store';
import {RootObject, Student, Address} from '../controllers/apiResponses/IAuth';

export function time_convert(time: number) {
  var mind = time % (60 * 60);
  var minutes = Math.floor(mind / 60);

  var secd = mind % 60;
  var seconds = Math.ceil(secd);
  return minutes + ':' + seconds;
}

export const ResetStack = (route: any, navigation: any) => {
  const resetAction = CommonActions.reset({
    index: 1,
    routes: [{name: route}],
  });
  navigation.dispatch(resetAction);
};

// export const isLoggedIn = () => {
//   const state = store.getState();
//   const authToken = (state.auth.data as RootObject)?.content?.accessToken;
//   if (authToken !== undefined && authToken !== null) {
//     return true;
//   } else {
//     return false;
//   }
// };

// export const getUserId = () => {
//   const state = store.getState();
//   const userId = (state.auth.data as RootObject)?.content?.student.user.id;
//   return userId;
// };

export const getMailingAddress = (student: Student) => {
  let mailingAddress = '';
  if (student.address.street !== null) {
    mailingAddress += student.address.street + ', ';
  }
  if (student.address.city !== null) {
    mailingAddress += student.address.city + ', ';
  }
  if (student.address.stateCode !== null) {
    mailingAddress += student.address.stateCode + ' ';
  } else {
    mailingAddress += student.address.state + ' ';
  }
  if (student.address.zip !== null) {
    mailingAddress += student.address.zip + ', ';
  }
  if (student.address.countryCode !== null) {
    mailingAddress += student.address.countryCode;
  } else {
    mailingAddress += student.address.country;
  }
  return mailingAddress;
};

export const getBillingAddress = (student: Student) => {
  if (student.billingAddress === null || student.billingAddress === '') {
    return '';
  }
  let billingAddress = '';
  if (student.billingAddress.street !== null) {
    billingAddress += student.billingAddress.street + ', ';
  }
  if (student.billingAddress.city !== null) {
    billingAddress += student.billingAddress.city + ', ';
  }
  if (student.billingAddress.stateCode !== null) {
    billingAddress += student.billingAddress.stateCode + ' ';
  } else {
    billingAddress += student.billingAddress.state + ' ';
  }
  if (student.billingAddress.zip !== null) {
    billingAddress += student.billingAddress.zip + ', ';
  }
  if (student.billingAddress.countryCode !== null) {
    billingAddress += student.billingAddress.countryCode;
  } else {
    billingAddress += student.billingAddress.country;
  }
  return billingAddress;
};

export const getAddressObject = (results: any) => {
  const addressComponents = results.address_components;
  const location = results.geometry.location;

  const shouldBeComponent = {
    home: ['street_number'],
    street: ['street_address', 'route'],
    country: ['country'],
    state: [
      'administrative_area_level_1',
      'administrative_area_level_2',
      'administrative_area_level_3',
      'administrative_area_level_4',
      'administrative_area_level_5',
    ],
    city: [
      'locality',
      'sublocality',
      'sublocality_level_1',
      'sublocality_level_2',
      'sublocality_level_3',
      'sublocality_level_4',
    ],
    zip: ['postal_code'],
  };

  const address = {
    home: '',
    street: '',
    country: {
      name: '',
      code: '',
    },
    state: {
      name: '',
      code: '',
    },
    city: '',
    zip: '',
  };

//   addressComponents.forEach(component => {
//     for (const shouldBe in shouldBeComponent) {
//       if (shouldBeComponent[shouldBe].indexOf(component.types[0]) !== -1) {
//         if (shouldBe === 'country' || shouldBe === 'state') {
//           address[shouldBe].name = component.long_name;
//           address[shouldBe].code = component.short_name;
//         } else {
//           address[shouldBe] = component.long_name;
//         }
//       }
//     }
//   });

  let mailingAddress: Address = {
    country: address.country.name,
    countryCode: address.country.code,
    state: address.state.name,
    stateCode: address.state.code,
    city: address.city,
    zip: address.zip,
    street: `${address.home} ${address.street}`,
    latitude: location.lat,
    longitude: location.lng,
  };

  return {
    mailingAddress,
  };
};

export const setAddressObject = (address: any) => {
  let billingAddress = '';
  if (address.street !== null) {
    billingAddress += address.street + ', ';
  }
  if (address.city !== null) {
    billingAddress += address.city + ', ';
  }
  if (address.stateCode !== null) {
    billingAddress += address.stateCode + ' ';
  } else {
    billingAddress += address.state + ' ';
  }
  if (address.zip !== null) {
    billingAddress += address.zip + ', ';
  }
  if (address.countryCode !== null) {
    billingAddress += address.countryCode;
  } else {
    billingAddress += address.country;
  }
  return billingAddress;
};
