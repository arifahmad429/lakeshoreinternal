export const APIConstants = {
    SignInURL: 'auth/signin/student',
    //BaseURL: 'http://learningcenter-api-demo.us-east-1.elasticbeanstalk.com/',
    BaseURL: 'https://demo-api.integrativenutrition.com/',
    Modules: '/cohorts/',
    ResetPassword: '/users/',
    UpdateUser: '/students/',
    Student: '/students/',
    GetFile: '/files/',
    GetConfiguration: '/public/lookups',
    Programs: '/programs/',
    Courses: '/cohorts?',
    ApplicationData: '/applications?',
    Communications: '/communications?',
    ForgotPassword: '/auth/student/forgot_password',
    CommunicationSubscription: '/communication_subscriptions/',
    Logout: '/logout',
  };
  