const Constants = {
    //Login Page
    EmailPlaceholder: 'Enter your Email',
    AccountPlaceholder: 'Enter your Account',
    PasswordPlaceholder: 'Enter your Password',
    SignInText: 'Sign In',
    JoinNow: 'Join Now',
    ContinueText: 'Continue',
    BackToLoginText: 'Back to Log In',
    ForgotPasswordText: 'Forgot Password?',
    SocialMediaText: 'Login with Social Media',
    LoginData: 'LoginData',
    Logout: 'Logout',
    Edit: 'Edit',
    UpdateUser: 'UpdateUser',
    Configuration: 'configuration',
    SaveCohorts: 'saveCohorts',
    StartText: "Let's get started",
    CurrentPasswordText: 'Current Password',
    PasswordText: 'Password',
    ConfirmPasswordText: 'Confirm Password',
    BackText: 'Back',
  };
  
  export {Constants};
  