import {
    downloadFile,
    sendPostRequest,
    sendPutRequest,
  } from '../../network/network';
  import {APIConstants} from '../../config/apiConstants';
  import {RootObject, IStudent, User} from '../apiResponses/IAuth';
  
  export const getToken = async (creds: any) => {
    return await sendPostRequest<RootObject>(APIConstants.SignInURL, creds);
  };
  
  export const resetPassword = async (password: string, userId: number, currentPassword: string) => {
    let data = {currentPassword: currentPassword, password: password};
    let result = await sendPutRequest<User>(
      APIConstants.ResetPassword + userId,
      data,
    );
    return result;
  };
  
  export const getFile = async (filename: string) => {
    let result = await downloadFile<Blob>(APIConstants.GetFile + filename);
    return result;
  };
  
  export const updateStudentInfo = async (user: any, userId: number) => {
    return await sendPutRequest<IStudent>(APIConstants.UpdateUser + userId, user);
  };
  
//   export const forgotPassword = async (email: any) => {
//     return await sendPostRequest<ForgotPasswordResponse>(
//       APIConstants.ForgotPassword,
//       email,
//     );
//   };
  
//   export const signOut = async (studentId: number) => {
//      let url: string = APIConstants.Student + studentId + APIConstants.Logout;
//      return await sendPostRequest<LogoutResponse>(url, null);
//   };
  