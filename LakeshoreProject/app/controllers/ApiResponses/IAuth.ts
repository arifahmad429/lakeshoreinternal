export interface Privilege {
    id: number;
    name: string;
    description: string;
  }
  
  export interface Privilege {
    id: number;
    name: string;
    description: string;
  }
  
  export interface Role {
    id: number;
    name: string;
    description: string;
    privileges: Privilege[];
  }
  
  export interface User {
    id: number;
    email: string;
    roles: Role[];
  }
  
  export interface PersonalInfo {
    firstName: string;
    middleName: string;
    lastName: string;
    gender?: any;
    dateBirth?: any;
  }
  
  export interface Address {
    country: string;
    countryCode?: any;
    state?: any;
    stateCode?: any;
    city?: any;
    zip?: any;
    street?: any;
    latitude?: any;
    longitude?: any;
  }
  
  export interface Student {
    id: number;
    user: User;
    personalInfo: PersonalInfo;
    contactInfo?: any;
    address: Address;
    billingAddress?: any;
    photo?: any;
    salesforceId: string;
    edluminaId?: any;
    d2lUserId?: any;
    brightspaceId?: any;
    preferredLanguage?: any;
    educationLevel?: any;
    occupation?: any;
    ssnLastFour?: any;
    militaryMember?: any;
    active?: any;
  }
  
  export interface IStudent {
    content: Student;
  }
  
  export interface Content {
    email: string;
    accessToken: string;
    refreshToken: string;
    administrator?: any;
    student: Student;
  }
  
  export interface RootObject {
    content?: Content;
  }
  