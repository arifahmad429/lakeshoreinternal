export interface Content {
    id: number;
    type: string;
    value: string;
  }
  
  export interface Configuration {
    content: Content[];
  }
  