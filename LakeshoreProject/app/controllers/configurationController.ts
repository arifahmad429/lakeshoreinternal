import {APIConstants} from '../config/apiConstants';
import {sendGetRequest} from '../network/network';

export const getConfiguration = async () => {
  return await sendGetRequest(APIConstants.GetConfiguration);
};
