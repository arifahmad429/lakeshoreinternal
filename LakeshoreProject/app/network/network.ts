const axios = require('react-native-axios');
import {APIConstants} from '../config/apiConstants';
import {IApiResponse} from '../controllers/apiResponses/IAPIResponse';
import {store} from '../redux/store';
import {RootObject} from '../controllers/apiResponses/IAuth';

const token = () => {
  const state = store.getState();
  const authToken = (state.auth as RootObject).content?.accessToken;
  return 'Bearer ' + authToken;
};
const instance = axios.create({
  baseURL: APIConstants.BaseURL,
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json',
    Accept: '*/*',
  },
});

// export function downloadFile<T>(url: string) {
//   return instance
//     .get(url, {
//       responseType: 'blob',
//     })
//     .then(response => {
//       console.log(response.data);
//       //const downloadUrl = window.URL.createObjectURL(new Blob([response.data]));
//       return handleResponse(response.data);
//     })
//     .catch((err: any) => {
//       if (err.response === undefined) {
//         console.log(err);
//         throw new Error('something went wrong');
//       }
//       return handleError(err.response.data);
//     })
//     .finally(() => {
//       // hide loader
//       // dispatch(toggleLoader());
//     });
// }

export function sendGetRequest<T>(url: string) {
  instance.defaults.headers.common.Authorization = token();
  return instance
    .get(url)
    .then((response: any) => {
      return handleResponse(response.data);
    })
    .catch((err: any) => {
      if (err.response === undefined) {
        throw new Error('something went wrong');
      }
      return handleError(err.response.data);
    })
    .finally(() => {
      // hide loader
      // dispatch(toggleLoader());
    });
}

export function sendPostRequest<T>(url: any, body: any): any {
  return instance
    .post(url, body)
    .then((response: any) => {
      return handleResponse(response.data);
    })
    .catch((err: any) => {
      console.log(err);
      if (err.response === undefined) {
        throw new Error('something went wrong');
      }
      return handleError(err.response.data);
    })
    .finally(() => {
      // hide loader
      //   dispatch(toggleLoader());
    });
}

export function sendPutRequest<T>(url: any, body: any): any {
  instance.defaults.headers.common.Authorization = token();
  return instance
    .put(url, body)
    .then((response: any) => handleResponse(response.data))
    .catch((err: any) => {
      if (err.response === undefined) {
        throw new Error('something went wrong');
      }
      return handleError(err.response.data);
    })
    .finally(() => {
      // hide loader
      //   dispatch(toggleLoader());
    });
}

export function deleteRequest<T>(url: any): any {
  return instance
    .delete(url)
    .then((response: any) => handleResponse(response.data))
    .catch((err: any) => {
      if (err.response === undefined) {
        throw new Error('something went wrong');
      }
      return handleError(err.response.data);
    })
    .finally(() => {
      // hide loader
      //   dispatch(toggleLoader());
    });
}

function handleResponse<T>(data: any) {
  let res: IApiResponse<T> = {
    isSuccess: true,
    errors: undefined,
    data: data as T,
  };
  return res;
}

function handleError<T>(data: any) {
  let res: IApiResponse<T> = {
    isSuccess: false,
    errors: data,
    data: undefined,
  };
  return res;
}
