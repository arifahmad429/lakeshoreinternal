import {Constants} from '../config/constants';
export const saveLoginData = (data: any) => {
  return {
    type: Constants.LoginData,
    payload: data,
  };
};
export const userLogout = () => {
  return {
    type: Constants.Logout,
    payload: null,
  };
};
export const saveConfiguration = (data: any) => {
  return {
    type: Constants.Configuration,
    payload: data,
  };
};
export const updateUser = (data: any) => {
  return {
    type: Constants.UpdateUser,
    payload: data,
  };
};
export const saveCohorts = (data: any) => {
  return {
    type: Constants.SaveCohorts,
    payload: data,
  };
};
