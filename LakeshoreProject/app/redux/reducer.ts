import {Constants} from '../config/constants';
/**
 * This is a reducer - a function that takes a current state value and an
 * action object describing "what happened", and returns a new state value.
 * A reducer's function signature is: (state, action) => newState
 *
 * The Redux state should contain only plain JS objects, arrays, and primitives.
 * The root state value is usually an object.  It's important that you should
 * not mutate the state object, but return a new object if the state changes.
 *
 * You can use any conditional logic you want in a reducer. In this example,
 * we use a switch statement, but it's not required.
 */
const initialState = {
  // data: {
  //   content: {
  //     email: '',
  //     accessToken: '',
  //     refreshToken: '',
  //     administrator: {
  //       id: 0,
  //       user: {
  //         id: 0,
  //         email: '',
  //         roles: [
  //           {
  //             id: 0,
  //             name: '',
  //             description: '',
  //             privileges: [
  //               {
  //                 id: 0,
  //                 operation: '',
  //                 description: '',
  //               },
  //             ],
  //           },
  //         ],
  //       },
  //       personalInfo: {
  //         firstName: '',
  //         middleName: '',
  //         lastName: '',
  //         gender: '',
  //         dateBirth: '',
  //       },
  //       contactInfo: {
  //         homePhone: '',
  //         workPhone: '',
  //         mobilePhone: '',
  //       },
  //       address: {
  //         country: '',
  //         countryCode: '',
  //         state: '',
  //         stateCode: '',
  //         city: '',
  //         zip: '',
  //         street: '',
  //         latitude: 0,
  //         longitude: 0,
  //       },
  //     },
  //     student: {
  //       id: 0,
  //       user: {
  //         id: 0,
  //         email: 'string',
  //         roles: [
  //           {
  //             id: 0,
  //             name: 'string',
  //             description: 'string',
  //             privileges: [
  //               {
  //                 id: 0,
  //                 operation: 'string',
  //                 description: 'string',
  //               },
  //             ],
  //           },
  //         ],
  //       },
  //       personalInfo: {
  //         firstName: '',
  //         middleName: '',
  //         lastName: '',
  //         gender: '',
  //         dateBirth: '',
  //       },
  //       contactInfo: {
  //         homePhone: '',
  //         workPhone: '',
  //         mobilePhone: '',
  //       },
  //       address: {
  //         country: '',
  //         countryCode: '',
  //         state: '',
  //         stateCode: '',
  //         city: '',
  //         zip: '',
  //         street: '',
  //         latitude: 0,
  //         longitude: 0,
  //       },
  //       billingAddress: {
  //         country: '',
  //         countryCode: '',
  //         state: '',
  //         stateCode: '',
  //         city: '',
  //         zip: '',
  //         street: '',
  //         latitude: 0,
  //         longitude: 0,
  //       },
  //       photo: '',
  //       salesforceId: '',
  //       edluminaId: 0,
  //       brightspaceId: 0,
  //       preferredLanguage: '',
  //       educationLevel: '',
  //       occupation: '',
  //       militaryMember: true,
  //       active: true,
  //     },
  //   },
  // },
  data: {
    content: {},
  },
  config: null,
  cohort: null,
};

// const data: RootObject = {
//   content: undefined,
// };

export function loginReducer(state = initialState, action: any) {
  switch (action.type) {
    case Constants.LoginData:
      return {data: action.payload};
    case Constants.Logout:
      return {data: initialState};
    case Constants.UpdateUser:
      return {
        ...state,
        data: {
          ...state.data.content,
          content: {
            ...state.data.content,
            student: action.payload,
          },
        },
      };
    default:
      return state;
  }
}

export function configurationReducer(state = initialState, action: any) {
  switch (action.type) {
    case Constants.Configuration:
      return {config: action.payload};
    default:
      return state;
  }
}

export function cohortsReducer(state = initialState, action: any) {
  switch (action.type) {
    case Constants.SaveCohorts:
      return {cohort: action.payload};
    default:
      return state;
  }
}
