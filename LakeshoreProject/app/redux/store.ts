import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunk from 'redux-thunk';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {persistStore, persistReducer} from 'redux-persist';

import {loginReducer, configurationReducer, cohortsReducer} from './reducer';

var rootReducer = combineReducers({
  auth: loginReducer,
  config: configurationReducer,
  cohort: cohortsReducer,
});

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['auth', 'config', 'cohort'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const middleware = applyMiddleware(thunk);
const store = createStore(persistedReducer, middleware);
export type RootState = ReturnType<typeof rootReducer>;
let persistor = persistStore(store, null, () => {
  store.getState();
});
export {store, persistor};
