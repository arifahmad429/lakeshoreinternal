import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import LoginViewModel from '../viewModel/loginViewModel';
// import DashboardScreen from '../views/dashboard/dashboard';
// import VideoPlayerScreen from '../views/videoPlayer/videoPlayer';
// import DefaultPlayerScreen from '../views/videoPlayer/defaultVideoPlayer';
// import VLCPlayerScreen from '../views/videoPlayer/vlcPlayer';
// import FeedbackScreen from '../views/feedback/feedback';
// import SettingsScreen from '../views/settings/settings';
// import ChangeLanguageScreen from '../views/changeLanguage/changeLanguage';
// import ProfileViewModel from '../viewModels/profileViewModel';
// import ChangePasswordScreen from '../viewModels/changePasswordViewModel';
// import ModuleViewModel from '../viewModels/moduleViewModel';
// import {isLoggedIn} from '../common/helpers';
// import D2LLoginViewModel from '../viewModels/d2lLoginViewModel';
// import ProgressScreen from '../views/progress/progress';
// import InboxViewModel from '../viewModels/inboxViewModel';
// import TestScreen from '../views/test/test';
// import CoachingCircleScreen from '../views/coachingCircles/coachingCircles';
// import HealthScreen from '../views/healthHistory/healthHistory';
// import MessageDetailViewModel from '../viewModels/messageDetailViewModel';
// import ForgotPasswordScreen from '../viewModels/forgotPasswordViewModel';
// import HelpScreen from '../views/help/help';
// import CommunicationPrefViewModel from '../viewModels/communicationPreferenceViewModel';

type RootStackParamList = {
  Login: undefined;
  Dashboard: undefined;
  VideoPlayer: undefined;
  DefaultPlayer: undefined;
  VLCPlayer: undefined;
  Feedback: undefined;
  Settings: undefined;
  ChangeLanguage: undefined;
  Profile: undefined;
  ChangePassword: undefined;
  Module: undefined;
  D2LLogin: undefined;
  Progress: undefined;
  Inbox: undefined;
  Test: undefined;
  Coaching: undefined;
  Health: undefined;
  MessageDetail: undefined;
  ForgotPassword: undefined;
  Help: undefined;
  Communication: undefined;
};
const RootStack = createStackNavigator<RootStackParamList>();
interface props {
  isLoggedIn: boolean;
}

const Routes = () => {
  return (
    <NavigationContainer>
      <RootStack.Navigator
        initialRouteName={'Login'}>
        <RootStack.Screen
          name="Login"
          component={LoginViewModel}
          options={{headerShown: false}}
        />
       
      </RootStack.Navigator>
    </NavigationContainer>
  );
};
export default Routes;
