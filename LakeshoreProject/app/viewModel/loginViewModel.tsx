import React, {useRef, useState} from 'react';
import {Platform} from 'react-native';
import {Alert, Animated, Easing} from 'react-native';
import {useDispatch} from 'react-redux';
import {ResetStack} from '../common/helper';
import { getToken } from '../controllers/ApiResponses/AccountController';
import {Configuration} from '../controllers/apiResponses/configurationResponse';
import {IApiResponse} from '../controllers/apiResponses/IAPIResponse';
import {RootObject} from '../controllers/apiResponses/IAuth';
import {getConfiguration} from '../controllers/configurationController';
import {saveConfiguration, saveLoginData} from '../redux/action';
import Login from '../views/login/login';

const LoginViewModel = ({navigation}: any) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [emailCloseIcon, setEmailCloseIcon] = useState(false);
  const [passwordCloseIcon, setPasswordCloseIcon] = useState(false);
  const [passwordInfoIcon, setPasswordInfoIcon] = useState(false);
  const [paddingEmail, setPaddingEmail] = useState(
    Platform.OS === 'ios' ? 30 : 0,
  );
  const [paddingEmailBottom, setPaddingEmailBottom] = useState(
    Platform.OS === 'ios'? 0 : 0,
  );
  const [paddingPassword, setPaddingPassword] = useState(
    Platform.OS === 'ios' ? 30 : 0,
  );
  const [paddingPasswordBottom, setPaddingPasswordBottom] = useState(
    Platform.OS === 'ios'? 0 : 0,
  );
  const [keyboard, setKeyboard] = useState(false);
  const [keyboardEmail, setKeyboardEmail] = useState(false);
  const [showError, setShowError] = useState(false);
  const [errorText, setErrorText] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const spinValue = useRef(new Animated.Value(0)).current;

  const saveLoginDetails = (res: any) => dispatch(saveLoginData(res));
  const saveConfig = (res: any) => dispatch(saveConfiguration(res));

  const animateLoader = () => {
    Animated.timing(spinValue, {
      toValue: 1,
      duration: 3000,
      easing: Easing.linear, // Easing is an additional import from react-native
      useNativeDriver: true, // To make use of native driver for performance
    }).start();
  };

  const spin = spinValue.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg'],
  });

  const updateEmailPadding = (val: any) => {
    if (val !== 0 && val !== 30) {
      setKeyboardEmail(true);
      setPaddingEmail(val);
    } else if (email.length === 0) {
      setKeyboardEmail(false);
      setPaddingEmail(val);
    } else {
      setKeyboardEmail(false);
    }
  };

  const updatePasswordPadding = (val: any) => {
    if (val !== 0 && val !== 30) {
      setPasswordInfoIcon(false);
      setShowError(false);
      setKeyboard(true);
      setPaddingPassword(val);
    } else if (password.length === 0) {
      setKeyboard(false);
      setPaddingPassword(val);
    } else {
      setKeyboard(false);
    }
  };

  const clearData = () => {
    setEmail('');
    setPassword('');
  };

  const onFaIconTapped = (type: number) => {
    switch (type) {
      case 1:
        updateEmail('');
        !keyboardEmail && setPaddingEmail(Platform.OS === 'ios' ? 30 : 0);
        !keyboardEmail && setPaddingEmailBottom(Platform.OS === 'ios' ? 0 : 0);
        break;
      case 2:
        updatePassword('');
        !keyboard && setPaddingPassword(Platform.OS === 'ios' ? 30 : 0);
        !keyboard && setPaddingPasswordBottom(Platform.OS === 'ios' ? 0 : 0);
        break;
      case 3:
        setShowError(!showError);
        break;
      case 4:
         setShowPassword(!showPassword);
        break;
    }
  };

  const updateEmail = (data: string) => {
    setEmail(data);
    setEmailCloseIcon(data.length > 0 ? true : false);
  };

  const updatePassword = (data: string) => {
    setPasswordInfoIcon(false);
    setPassword(data);
    setPasswordCloseIcon(data.length > 0 ? true : false);
  };

  const onLogin = async () => {
    animateLoader();
    setLoading(true);
    let id: string = email.toLowerCase();
    var res: IApiResponse<RootObject> = await getToken({email : id, password});
    var res: IApiResponse<RootObject>
    if (res.isSuccess) {
      setShowError(false);
      var content = res.data;
      saveLoginDetails(content);
      var configRes: IApiResponse<Configuration> = await getConfiguration();
    setLoading(false);
      if (configRes.isSuccess) {
        saveConfig(configRes.data);
      } else {
        Alert.alert('Error', configRes.errors!.error.message);
      }
      clearData();
      ResetStack('Dashboard', navigation);
    } else {
      setLoading(false);
      let msg = res.errors!.error.message;
      setErrorText(msg);
      setPasswordInfoIcon(true);
      setPasswordCloseIcon(false);
      //Alert.alert('Error', msg);
    }
  };
  return (
    <Login/>
  );
};

export default LoginViewModel;
