import React from 'react';
import {
  Text,
  View,
  Image,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import {Glyphs} from '../../config/glyphs';
import {Constants} from '../../config/constants';
import {styles} from './style';

const Login = ({
}) => {
  return (
    <SafeAreaView style={{backgroundColor: 'white'}}>
    <ScrollView
      contentInsetAdjustmentBehavior="automatic">
      {/* <View
        style={{ padding: 10, flex: 1,
          flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: 'grey'
        }}>
        <Image source={Glyphs.appleLogo} style={{height: 15, width: 15}}/>
        <View style={{alignItems: 'center'}}>
        <Text style={{fontWeight: 'bold'}}>App bar title</Text>
        <Text>App bar subTitle</Text>
        </View>
        <Text style={{color: '#4da2c3'}}>{Constants.Edit}</Text>
      </View> */}
      <Image source={Glyphs.welcome1} style={{height: 170, marginRight: 10, width: '100%'}}/>
      <Image source={Glyphs.welcome2} style={{height: 300, padding: 10, width: '100%'}}/>
      <Text style={{color: '#1b4878', fontSize: 36, marginTop: 10, textAlign: 'center', fontWeight: 'bold'}}>Earn Points.</Text>
      <Text style={{color: '#1b4878', fontSize: 36, textAlign: 'center', fontWeight: 'bold'}}>Get Perks.</Text>
      <Text style={{color: '#1b4878', fontSize: 18, textAlign: 'center', fontWeight: 'bold'}}>Start saving today!</Text>
        <View style={{margin: 10, backgroundColor: 'red', height: 45, alignItems: 'center', justifyContent: 'center'}}>
          <Text style={{color: 'white', fontWeight: 'bold'}}>{Constants.JoinNow}</Text>
        </View>
        <View style={{margin: 10, backgroundColor: 'white', height: 45, alignItems: 'center', borderColor: '#1b4878', borderWidth: 1, justifyContent: 'center'}}>
          <Text style={{color: '#1b4878', fontWeight: 'bold'}}>{Constants.SignInText}</Text>
        </View>
      <View style={{justifyContent: 'space-between', marginTop: 10, alignItems: 'center'}}>
        <Text style={{color: '#1b4878', fontWeight: 'bold'}}>Not Now</Text>
        </View>
    </ScrollView>
  </SafeAreaView>
  );
};

export default Login;
