import {StyleSheet, Dimensions, Platform} from 'react-native';

export const styles = () =>
  StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'rgba(248, 245, 240, 1)',
    },
    logoContainer: {
      flex: 0.2,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'rgba(248, 245, 240, 1)',
      paddingTop: 10,
    },
    innerContainer: {
      flex: 0.8,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'flex-start',
      backgroundColor: 'rgba(248, 245, 240, 1)',
    },
    button: {
      backgroundColor: 'rgba(45, 56, 65, 1)',
      width: '80%',
      height: 44,
      justifyContent: 'center',
      alignItems: 'center',
      borderColor: 'white',
    },
    buttonText: {
      fontSize: 16,
      fontFamily: 'PublicSans-SemiBold',
      justifyContent: 'center',
      color: '#EAE2D3',
    },
    inputEmail: {
      //paddingTop: Platform.OS === 'ios' && email.length > 0 ? 20 : 0,
      paddingHorizontal: 10,
      width: '80%',
      height: Platform.OS === 'ios' ? 50 : 60,
      borderWidth: 1,
      borderColor: '#D3D3D3',
      backgroundColor: 'white',
      color: 'black',
      justifyContent: Platform.OS === 'ios' ? 'center' : 'flex-start',
    },
    inputPassword: {
      paddingHorizontal: 10,
      width: '80%',
      height: Platform.OS === 'ios' ? 50 : 60,
      borderWidth: 1,
      borderColor: '#D3D3D3',
      backgroundColor: 'white',
      color: 'black',
      justifyContent: Platform.OS === 'ios' ? 'center' : 'flex-start',
    },
    inputContainer: {
      width: '100%',
      flexDirection: 'row',
      justifyContent: 'center',
    },
    viewGap20: {
      width: '100%',
      height: 20,
      backgroundColor: 'rgba(248, 245, 240, 1)',
    },
    viewGap30: {
      width: '100%',
      height: 30,
      backgroundColor: 'rgba(248, 245, 240, 1)',
    },
    viewGap50: {
      width: '100%',
      height: 50,
      backgroundColor: 'rgba(248, 245, 240, 1)',
    },
    viewGap: {
      width: 20,
      backgroundColor: 'rgba(248, 245, 240, 1)',
    },
    viewGap5: {
      height: 5, 
    },
    image: {
      width: 126.48,
      height: 28,
    },
    textStyle: {
      color: 'rgba(23, 29, 34, 0.5)',
      fontSize: 12,
      fontFamily: 'PublicSans-Light',
      alignSelf: 'flex-end',
      paddingRight: 40,
    },
    socialMediaView: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'flex-start',
      justifyContent: 'center',
    },
    touchableContainer: {
      alignItems: 'center',
      justifyContent: 'center',
    },
    socialMediaImage: {
      width: 45,
      height: 45,
    },
    socialMediaTextStyle: {
      height: 40,
      color: 'black',
      fontSize: 15,
    },
    loaderContainer: {
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      position: 'absolute',
      top: 0,
      left: 0,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#00000080',
    },
    loadingText: {
      paddingTop: 20,
      fontSize: 22,
      color: 'white',
      paddingLeft: 15,
    },
    welcomeText: {
      fontSize: 32,
      fontFamily: 'PublicSans-SemiBold',
      color: 'rgba(23, 29, 34, 1)',
    },
    faIconStyle: {
      position: 'absolute',
      top: Platform.OS === 'ios' ? 17 : 22,
      right: 50,
    },
    pinIconStyle: {
      flexDirection: 'row',
      position: 'absolute',
      top: Platform.OS === 'ios' ? 17 : 22,
      right: 50,
    },
    faImageStyle: {
      height: 15,
      width: 15,
    },
    sideIconView: {
      flexDirection: 'row',
    },
    emptyView: {
      width: 10,
    },
    errorText: {
      color: '#D72A25',
      fontSize: 12,
      fontWeight: '300',
      alignSelf: 'flex-start',
      paddingLeft: 50,
      paddingRight: 40,
    },
  });
